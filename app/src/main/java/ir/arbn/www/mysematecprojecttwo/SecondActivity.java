package ir.arbn.www.mysematecprojecttwo;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class SecondActivity extends AppCompatActivity {
    EditText Username;
    EditText Password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Username = findViewById(R.id.Username);
        Password = findViewById(R.id.Password);

        findViewById(R.id.Register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String usernameValue = Username.getText().toString();
                String passwordValue = Password.getText().toString();
                Intent backIntent = new Intent();
                if (usernameValue.length() == 0 || passwordValue.length() == 0) {
                    setResult(Activity.RESULT_CANCELED, backIntent);
                } else {
                    backIntent.putExtra("Username", "Your Username: " + usernameValue);
                    backIntent.putExtra("Password", "Your Password: " + passwordValue);
                    setResult(Activity.RESULT_OK, backIntent);
                }

                finish();
            }

        });
    }
}